package com.homework_linkedlists.nodes;

import java.util.Objects;

public class Node2<T>{
    public T item;
    public Node2<T> prev;
    public Node2<T> next;

    public Node2(T item, Node2<T> prev, Node2<T> next) {
        this.item = item;
        this.prev = prev;
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node2<?> node2 = (Node2<?>) o;
        return Objects.equals(item, node2.item) && Objects.equals(next, node2.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, prev, next);
    }
}