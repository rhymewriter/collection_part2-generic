package com.homework_linkedlists.nodes;

import java.util.Objects;

public class Node1<T>{
    public T item;
    public Node1<T> next;

    public Node1(T item, Node1<T> next) {
        this.item = item;
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node1<?> node1 = (Node1<?>) o;
        return Objects.equals(item, node1.item) && Objects.equals(next, node1.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, next);
    }
}