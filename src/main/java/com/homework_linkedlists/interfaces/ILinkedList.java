package com.homework_linkedlists.interfaces;

public interface ILinkedList<T> {

    void clear();

    int size();

    T get(int index);

    boolean add(T value);

    boolean addFirst(T value);

    boolean addLast(T value);

    boolean add(int index, T value);

    T remove(T element);

    T removeFirst();

    T removeLast();

    T removeByIndex(int index);

    boolean contains(T value);

    boolean set(int index, T value);

    void print(); //=> выводит в консоль массив в квадратных скобка и через запятую

    Object[] toArray(); //=> приводит данные к массиву, в случае с AList ничего сложного, у нас и так массив

    boolean removeAll(T[] arr);

}
