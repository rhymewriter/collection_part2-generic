package com.homework_linkedlists.implementations;



import com.homework_linkedlists.interfaces.ILinkedList;
import com.homework_linkedlists.nodes.Node2;

import java.util.*;

public class LList2<T> implements ILinkedList<T> {
    private int size = 0;
    private Node2<T> first;
    private Node2<T> last;

    public LList2(){

    }

    public LList2(T[] array) {
        for (int i = 0; i < array.length; i++) {
            add(array[i]);
        }
    }

    public LList2(Collection<T> collection){
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            T element = iterator.next();
            add(element);
            size++;
        }
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (index == size - 1) {
            return last.item;
        }
        else if (index == 0) {
            return first.item;
        }
        else {
            T itemToGet = null;
            Node2<T> node2ToGet = first;
            for (int i = 1; i < size - 1; i++) {
                node2ToGet = node2ToGet.next;
                if (i == index) {
                    itemToGet = node2ToGet.item;
                }
            }
            return itemToGet;
        }
    }

    @Override
    public boolean add(T value) {
        return addLast(value);
    }

    @Override
    public boolean addFirst(T value) {
        if (value == null) {
            return false;
        }
        else if (size == 0) {
            Node2<T> node2ToAdd = new Node2<>(value, null, null);
            first = node2ToAdd;
            last = node2ToAdd;
            size++;
            return true;
        }
        else {
            Node2<T> node2ToAdd = new Node2<>(value, null, first);
            first.prev = node2ToAdd;
            first = node2ToAdd;
            size++;
            return true;
        }
    }

    @Override
    public boolean addLast(T value) {
        if (value == null) {
            return false;
        }
        else if (size == 0) {
            Node2<T> node2ToAdd = new Node2<>(value, null, null);
            first = node2ToAdd;
            last = node2ToAdd;
            size++;
            return true;
        }
        else {
            Node2<T> node2ToAdd = new Node2<>(value, last, null);
            last.next = node2ToAdd;
            last = node2ToAdd;
            size++;
            return true;
        }
    }

    @Override
    public boolean add(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index > size) {
            return false;
        }
        else {
            if (index == 0) {
                return addFirst(value);
            }
            else if (index == size) {
                return addLast(value);
            }
            else {
                Node2<T> prevNode2 = first;
                Node2<T> nextNode2 = null;
                for (int i = 1; i < size; i++) {
                    nextNode2 = prevNode2.next;
                    if (i == index) {
                        Node2<T> newNode2 = new Node2<>(value, prevNode2, nextNode2);
                        prevNode2.next = newNode2;
                        newNode2.prev = newNode2;
                        break;
                    }
                    prevNode2 = nextNode2;
                }
                size++;
                return true;
            }
        }
    }

    @Override
    public T remove(T element) {
        if (element == null) {
            return null;
        }
        else if (first.item.equals(element)) {
            return removeFirst();
        }
        else if (last.item.equals(element)) {
            return removeLast();
        }
        else {
            T valToReturn = null;
            Node2<T> prevNode2 = first;
            Node2<T> nextNode2 = null;
            for (int i = 1; i < size; i++) {
                nextNode2 = prevNode2.next;
                if (nextNode2.item.equals(element)) {
                    valToReturn = nextNode2.item;
                    Node2<T> newNextNode2 = nextNode2.next;
                    prevNode2.next = newNextNode2;
                    newNextNode2.prev = prevNode2;
                    size--;
                    break;
                }
                prevNode2 = nextNode2;
            }
            return valToReturn;
        }
    }

    @Override
    public T removeFirst() {
        if (size == 0) {
            return null;
        }
        else {
            T valueToRemove = first.item;
            if (size == 1) {
                first = null;
                last = null;
            }
            else {
                Node2<T> nextNode2 = first.next;
                first = nextNode2;
                first.prev = null;
            }
            size--;
            return valueToRemove;
        }
    }

    @Override
    public T removeLast() {
        if (size == 0) {
            return null;
        }
        else {
            T valueToRemove = last.item;
            if (size == 1) {
                first = null;
                last = null;
            }
            else {
                Node2<T> prevNode2 = last.prev;
                last = prevNode2;
                last.next = null;
            }
            size--;
            return valueToRemove;
        }
    }

    @Override
    public T removeByIndex(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (index == 0) {
            return removeFirst();
        }
        else if (index == size - 1) {
            return removeLast();
        }
        else {
            T valToReturn = null;
            Node2<T> prevNode2 = first;
            Node2<T> nextNode2 = null;
            for (int i = 1; i < size; i++) {
                nextNode2 = prevNode2.next;
                if (i == index) {
                    valToReturn = nextNode2.item;
                    Node2<T> newNextNode2 = nextNode2.next;
                    prevNode2.next = newNextNode2;
                    newNextNode2.prev = prevNode2;
                    size--;
                    break;
                }
                prevNode2 = nextNode2;
            }
            return valToReturn;
        }
    }

    @Override
    public boolean contains(T value) {
        if (value == null) {
            return false;
        }
        else if (last.item.equals(value)) {
            return true;
        }
        else if (first.item.equals(value)) {
            return true;
        }
        else{
            boolean doesContain = false;
            Node2<T> currentNode2 = first;
            for (int i = 1; i < size - 1; i++) {
                currentNode2 = currentNode2.next;
                if (currentNode2.item.equals(value)) {
                    doesContain = true;
                    break;
                }
            }
            return doesContain;
        }
    }

    @Override
    public boolean set(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index >= size) {
            return false;
        }
        else if (index == size - 1) {
            last.item = value;
            return true;
        }
        else if (index == 0) {
            first.item = value;
            return true;
        }
        else {
            Node2<T> currentNode2 = first;
            for (int i = 1; i < size; i++) {
                currentNode2 = currentNode2.next;
                if (i == index) {
                    currentNode2.item = value;
                    break;
                }
            }
            return true;
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        if (size != 0) {
            T valToAdd = null;
            Node2<T> prevNode2 = first;
            valToAdd = first.item;
            stringArray += valToAdd;
            Node2<T> nextNode2 = null;
            for (int i = 1; i < size; i++) {
                stringArray += ", ";
                nextNode2 = prevNode2.next;
                stringArray += nextNode2.item;
                prevNode2 = nextNode2;
            }
        }
        stringArray += "]\n";
        System.out.println(stringArray);
    }

    @Override
    public Object[] toArray() {
        Object[] listArray = new Object[size];
        if (size != 0) {
            Node2<T> currentNode2 = first;
            listArray[0] = currentNode2.item;
            for (int i = 1; i < size; i++) {
                currentNode2 = currentNode2.next;
                listArray[i] = currentNode2.item;
            }
        }
        return listArray;
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null) {
            return false;
        }
        else {
            for (int i = 0; i < arr.length; i++) {
                remove(arr[i]);
            }
            return true;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LList2<?> lList2 = (LList2<?>) o;
        return size == lList2.size && Objects.equals(first, lList2.first) && Objects.equals(last, lList2.last);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, first, last);
    }
}
